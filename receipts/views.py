from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
# Create your views here.


# def home(request):
#     receipt = Receipt.objects.all()
#     category = ExpenseCategory.objects.all()
#     account = Account.objects.all()
#     context = {
#         "receipt": receipt,
#         "category": category,
#         "account": account,
#     }
#     return render(request, "receipts/home.html", context)

@login_required
def home(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    # category = ExpenseCategory.objects.all()
    # account = Account.objects.all()
    context = {
        "receipt": receipt,
        # "category": category,
        # "account": account,
    }
    return render(request, "receipts/home.html", context)

# def home(request):
#     receipt = get_object_or_404(Receipt)
#     category = get_object_or_404(Account)
#     account = get_object_or_404(ExpenseCategory)
#     context = {
#         "receipt": receipt,
#         "category": category,
#         "account": account,
#     }

#     return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


def categories(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories
    }

    return render(request, "receipts/categories.html", context)


def accounts(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts
    }

    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
