from django.db import models

# Create your models here.


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        "auth.User",
        related_name="categories",
        on_delete=models.CASCADE,
        null=True
    )


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        "auth.User",
        related_name="accounts",
        on_delete=models.CASCADE,
        null=True
     )


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        "auth.User",
        related_name="receipts",
        on_delete=models.CASCADE,
     )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    # should I take out null=true if the instructions don't specify that it should be there?
    # Should we migrate before or after test
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True
    )
