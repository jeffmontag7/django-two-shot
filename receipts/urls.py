from django.urls import path
from receipts.views import home, create_receipt, categories, accounts, create_category, create_account


urlpatterns = [
    path("", home, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", categories, name="category_list"),
    path("accounts/", accounts, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
